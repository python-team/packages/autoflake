autoflake (2.3.1-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Wed, 20 Mar 2024 16:38:20 +0000

autoflake (2.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright year.
  * Drop 0001-fix-for-tests-on-python-3.12.patch, applied upstream.

 -- Edward Betts <edward@4angle.com>  Tue, 27 Feb 2024 08:42:20 +0000

autoflake (2.2.1-2) unstable; urgency=medium

  * Fix tests to work with Python 3.12. (Closes: #1056229, #1058393)

 -- Edward Betts <edward@4angle.com>  Sun, 24 Dec 2023 11:12:04 +0000

autoflake (2.2.1-1) unstable; urgency=medium

  * New upstream release.
  * Upstream changes mean we no longer need to remove py.typed after
    dh_auto_install

 -- Edward Betts <edward@4angle.com>  Thu, 07 Sep 2023 20:01:53 +0100

autoflake (2.2.0-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Thu, 29 Jun 2023 15:25:51 +0200

autoflake (2.1.1-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Wed, 19 Apr 2023 21:13:43 +0100

autoflake (2.0.2-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Fri, 17 Mar 2023 16:59:06 +0100

autoflake (2.0.1-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright year.
  * Update Standards-Version.

 -- Edward Betts <edward@4angle.com>  Wed, 01 Feb 2023 20:25:51 +0000

autoflake (2.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Tue, 29 Nov 2022 08:58:02 +0000

autoflake (1.7.7-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Wed, 26 Oct 2022 12:00:54 +0100

autoflake (1.7.6-1) unstable; urgency=medium

  * New upstream release.
  * Remove DH_VERBOSE=1 from debian/rules.
  * Use python3-argparse-manpage to generate the manpage.
  * Upstream switched from setuptools to hatchling for the build system.
  * Add override_dh_auto_install to remove some files installed by upstream.

 -- Edward Betts <edward@4angle.com>  Sat, 15 Oct 2022 09:00:20 +0100

autoflake (1.6.1-2) unstable; urgency=medium

  * Needs python3-setuptools (>= 61.2) to build.

 -- Edward Betts <edward@4angle.com>  Sun, 02 Oct 2022 05:35:15 +0100

autoflake (1.6.1-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Mon, 26 Sep 2022 17:55:41 +0100

autoflake (1.5.3-1) unstable; urgency=medium

  * New upstream release.
  * Adjust patching of tests in autopkgtest for upstream changes.

 -- Edward Betts <edward@4angle.com>  Fri, 02 Sep 2022 08:19:33 +0100

autoflake (1.5.1-1) unstable; urgency=medium

  * New upstream release.
  * Drop patch to skip failing test on Python 3.10, fixed upstream.
  * Add Build-Depends on python3-toml for tests.

 -- Edward Betts <edward@4angle.com>  Mon, 29 Aug 2022 14:58:03 +0100

autoflake (1.4-2) unstable; urgency=medium

  * Add <!nocheck> to python3-pyflakes Build-Depends.
  * List test files in debian/pybuild.testfiles instead of debian/rules.
  * Install README.rst and AUTHORS.rst.

 -- Edward Betts <edward@4angle.com>  Fri, 05 Aug 2022 12:00:32 +0100

autoflake (1.4-1) unstable; urgency=medium

  * Initial release. (Closes: #1015318)

 -- Edward Betts <edward@4angle.com>  Tue, 19 Jul 2022 14:28:39 +0100
