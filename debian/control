Source: autoflake
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Edward Betts <edward@4angle.com>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               pybuild-plugin-pyproject,
               python3-all,
               python3-argparse-manpage,
               python3-hatchling,
               python3-poetry-core,
               python3-pyflakes <!nocheck>,
               python3-pytest <!nocheck>,
               python3-toml <!nocheck>
Rules-Requires-Root: no
Standards-Version: 4.6.2
Homepage: https://github.com/PyCQA/autoflake
Vcs-Browser: https://salsa.debian.org/python-team/packages/autoflake
Vcs-Git: https://salsa.debian.org/python-team/packages/autoflake.git

Package: autoflake
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Removes unused imports and unused variables in Python code
 By default, autoflake only removes unused imports for modules that are part of
 the standard library. (Other modules may have side effects that make them
 unsafe to remove automatically.) Removal of unused variables is also disabled
 by default.
 .
 Also removes useless 'pass' statements.
